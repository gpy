#!/usr/local/bin/perl
use strict;
use warnings;
use Bot::BasicBot::Pluggable;

my $bot = Bot::BasicBot::Pluggable->new(
 
                    channels => ["#wikinews-spam"],
                    server   => "irc.gnu.org",
                    port     => "6667",
 
                    nick     => "wnbot",
                    altnicks => ["pbot", "pluggable"],
                    username => "bot",
                    name     => "bot http://repo.or.cz/w/gpy.git",
 
              );

$bot->load('WikiLinker');
$bot->run();

#_ _END_ _

