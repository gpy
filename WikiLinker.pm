#!/usr/local/bin/perl

package Bot::BasicBot::Pluggable::Module::WikiLinker;
use Bot::BasicBot::Pluggable::Module;
use base qw(Bot::BasicBot::Pluggable::Module);

use strict;
use warnings;
use MediaWiki::API;
use Data::Dumper;
use WikiLinkParser;

my %channels_urls = (
    '#wikinews-spam'      => 'https://en.wikinews.org/w/api.php',
    '#wikipedia-ru-spam'  => 'https://ru.wikipedia.org/w/api.php',
);
my %mw;

# Secondary variables.
for my $channel_name (keys %channels_urls) {
    $mw{$channel_name} = MediaWiki::API->new();
    $mw{$channel_name}->{config}->{api_url} = $channels_urls{$channel_name};
}

sub told{
    shift->process_message(@_);
}

sub emoted {
    shift->process_message(@_);
}

sub process_message{
    my ($self, $msg, $pri) = @_;
    my $body = $msg->{body};
    my $who  = $msg->{who};
    my $channel  = $msg->{'channel'};
    if (!grep { $_ eq $channel } keys %channels_urls){
        return;
    }
    if ($body =~ m{\[\[(.*?)\]\]} or $body =~ m{\{\{(.*?)\}\}}g){
        return join " ", @{WikiLinkParser->get_urls_by_text($body, $mw{$channel})};
    }
    return;
}

sub help{
return "parser for [[*]] in messages";
}

1;

#_ _END_ _

