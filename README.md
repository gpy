- Note: WikinewsSpam.pm is a custom-made module to report a recent review queue submissions for Wikinews. You will normally not need it.
- WikiLinker.pm automatically parses [[]] and {{}} links and returns a list of full https URLs.

---------------
1) Edit bot.pl to specify nick and network.
2) Edit WikinewsSpam.pm file top to specify time (if you load this module).
3) perl -Ilib bot.pl 

--------------
When making changes to the parser in lib/, make sure all tests pass.
prove -Ilib -vm t/WikiLinkParser.pm
